import React, {Component} from 'react';
import PropTypes from 'prop-types';

import TextField from 'material-ui/TextField';

class SearchBox extends Component {
    onSearchChange = (event, value) => {
        this.props.onChange(value);
    };

    render() {
        const {value, onChange} = this.props;
        return (
            <div className="col-md-4 col-centered" style={{marginTop: 10}}>
                    <i className="material-icons form-control-feedback">search</i>

                    <TextField className=""
                           type="search"
                           value={value}
                           id="searchBox"
                           onChange={this.onSearchChange}
                    />
            </div>
        );
    }
}

SearchBox.propTypes = {};
SearchBox.defaultProps = {};

export default SearchBox;
