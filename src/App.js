import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import _ from 'lodash';

import SearchBox from './SearchBox';
import NewsList from './NewsList';
import Pagination from "./Pagination";

class App extends Component {
    state = {
        query : '',
        totalPages : undefined,
        currentPage: undefined,
        news: [],
        searching: false,
    };

    onQueryChange = (query) => {
        this.setState({query});
        if (!query) {
            this.setState({pages: undefined, currentPage: undefined, news:[]})
        } else {
            this.search(query, 0);
        }
    };

    _search = (query, page=0) => {
        this.setState({search: true});
        let url = `http://hn.algolia.com/api/v1/search?query=${query}&tags=story`;
        if (page > 0) {
            url += `&page=${page}`;
        }

        fetch(url)
            .then(response => response.json())
            .then(json => {
                const {nbPages: totalPages, hits:news} = json;
                this.setState({totalPages, currentPage: page, news, searching: false});
            })
            .catch(error => {
                this.setState({searching: false});
            })
    };

    search = _.debounce(this._search, 300);


    searchPreviousPage = () => {
        this.search(this.state.query, this.state.currentPage - 1);
    };

    searchNextPage = () => {
        this.search(this.state.query, this.state.currentPage + 1);
    };

    render() {
        const {query, news, currentPage, totalPages, searching} = this.state;
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">Welcome to Hacker news search</h1>
                </header>

                <SearchBox onChange={this.onQueryChange} value={query} />

                <NewsList news={news} />

                <Pagination
                    currentPage={currentPage}
                    totalPages={totalPages}
                    onPrevious={this.searchPreviousPage}
                    onNext={this.searchNextPage}
                    searching={searching}
                />

            </div>
        );
    }
}

export default App;
