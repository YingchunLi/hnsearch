import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {List, ListItem} from 'material-ui/List';

class NewsList extends Component {
    render() {
        const {news} = this.props;
        return (
            <div style={{width: '90%', margin: 'auto', height: 600, overflow: 'auto'}}>
            <List>
                {news.map((item, index) =>
                    <ListItem primaryText={item.title} key={index} />
                )}
            </List>
            </div>
        );
    }
}

NewsList.propTypes = {};
NewsList.defaultProps = {};

export default NewsList;
