import React, {Component} from 'react';
import PropTypes from 'prop-types';

import RaisedButton from 'material-ui/RaisedButton';

class Pagination extends Component {
    render() {
        const {currentPage, totalPages, onPrevious, onNext, searching} = this.props;

        if (!totalPages || currentPage === undefined) return null;

        return (
            <div style={{marginTop: 20}}>
                <RaisedButton label="Previous" onClick={onPrevious} disabled={currentPage === 0 || searching} />
                {`   Page ${currentPage +1} of ${totalPages}   `}
                <RaisedButton label="Next" onClick={onNext} disabled={currentPage + 1 === totalPages || searching} />
            </div>
        );
    }
}

Pagination.propTypes = {};
Pagination.defaultProps = {};

export default Pagination;
